# td4_flutter

## Equipe 
Fouqué Corentin
Bernardi Vincent

## Description du projet
Poursuite du développement des TD2 et 3.

## Fonctions implémentées

- Utilisation d'un formulaire pour ajouter une tâche
- Edition et suppression d'une tâche
- Utilisation du TaskViewModel pour le CRUD des tâches
- Possbilité d'ajouter une tâche en tant que favoris