import 'package:flutter/material.dart';
import 'package:td4_2223/home.dart';
import 'package:td4_2223/mytheme.dart';
import 'package:provider/provider.dart';
import 'package:td4_2223/viewmodels/settingviewmodel.dart';
import 'package:td4_2223/viewmodels/task_view_model.dart';


void main() {
  runApp(const MyTD2());
}

class MyTD2 extends StatelessWidget {
  const MyTD2({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) {
          SettingViewModel settingViewModel = SettingViewModel();
          //getSettings est deja appelee dans le constructeur
          return settingViewModel;
        },),
        ChangeNotifierProvider(create: (_) {
          TaskViewModel taskViewModel = TaskViewModel();
          taskViewModel.generateTasks();
          return taskViewModel;
        },),
      ],
      child: Consumer<SettingViewModel>(
        builder: (context, SettingViewModel notifier, child) {
          return MaterialApp(
              theme: notifier.isDark ? MyTheme.dark() : MyTheme.light(),
              title: 'TD4',
              debugShowCheckedModeBanner: false,
              home: const Home());
        },
      ),
    );
  }
}


// flutter pub add google_font