import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:td4_2223/viewmodels/task_view_model.dart';

import './models/task.dart';

class Edit extends StatelessWidget{


    Task task;
    Edit({required this.task});

    String? _titre;
    String? _tags;
    String? _nbhours; //int
    String? _difficulty; // int
    String? _description;

    final _formKey = GlobalKey<FormState>();

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Text(
                    'Edition de la tâche',
                    style: Theme.of(context).textTheme.titleLarge,
                ),
            ),
            body:
              Container(alignment: Alignment.center,
                child : Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                        Text('Titre : ${task.title}', style: const TextStyle(height: 3, fontSize: 30)),
                        SizedBox( // equivalent de Container
                          width: 200, 
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 25, color: Colors.green),
                            
                            onSaved: (value) {
                              _titre = value;
                            },

                          ), 
                        ),

                        Text('Tags : ${task.tags}', style: const TextStyle(height: 3, fontSize: 30)),
                        SizedBox(
                          width: 300, 
                          child: TextFormField(
                            decoration: const InputDecoration(labelText: 'Séparer les tags par une virgule',),
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 18, color: Colors.green),
                            
                            onSaved: (value) {
                              _tags = value;
                            },
                          ), 
                        ),
                        Text('Nombre d\'heure(s) : ${task.nbhours}', style: const TextStyle(height: 3, fontSize: 30)),
                        SizedBox(
                          width: 325, 
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 25, color: Colors.green),
                            
                            onSaved: (value) {
                              _nbhours = value;
                            },
                          ), 
                        ),
                        Text('Difficulté : ${task.difficulty}', style: const TextStyle(height: 3, fontSize: 30)),
                        SizedBox(
                          width: 200, 
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 25, color: Colors.green),
                            
                            onSaved: (value) {
                              _difficulty = value;
                            },
                          ), 
                        ),
                        Text('Description : ${task.description}', style: const TextStyle(height: 3, fontSize: 30)),
                        SizedBox(
                          width: 450, 
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 25, color: Colors.green),
                            
                            onSaved: (value) {
                              _description = value;
                            },
                          ), 
                        ),

                        ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              _formKey.currentState!.save();

                              // on regarde quel sont les infos remplies
                              if (_titre == ""){
                                _titre = null;                                
                              }if (_tags == "") {
                                _tags = null;
                              }if (_nbhours == "") {
                                _nbhours = null;
                              }if (_difficulty == "") {
                                _difficulty = null;
                              }if (_description == "") {
                                _description = null;
                              }

                              //on créer notre Tâche avec les nouvelles informations
                              Task modifiedTask = Task(
                                id: task.id,
                                title: _titre ?? task.title,
                                tags: _tags?.split(',') ?? task.tags,
                                nbhours: int.tryParse(_nbhours ?? task.nbhours.toString()) ?? task.nbhours,
                                difficulty: int.tryParse(_difficulty ?? task.difficulty.toString()) ?? task.difficulty,
                                description: _description ?? task.description,
                                favorite: task.favorite
                              );
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text('Modification de la tâche effectué !'),
                                  backgroundColor: Colors.lightGreen,
                                  ),
                              );

                              // on met à jour la liste
                              context.read<TaskViewModel>().editTask(modifiedTask);

                              // on revient à l'acceuil
                              Navigator.pop(context);
                            }
                          },
                          child: const Text('Enregistrer'),
                        )

                    ]
                )
              )
            ),
        );
    }
}