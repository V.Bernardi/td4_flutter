import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:td4_2223/viewmodels/task_view_model.dart';
import './models/task.dart';

// Define a custom Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({super.key});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Define a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {

  String? _titre;
  String? _tags;
  String? _nbhours; //int
  String? _difficulty; // int
  String? _description;

  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
  return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Formulaire de création d\'une tâche'),
      ),
      body: 
        Container(alignment: Alignment.center,
          child :
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  const Text('Entrez le titre de la tâche', style: TextStyle(height: 3, fontSize: 30)),
                  SizedBox( // equivalent de Container
                    width: 200, 
                    child: TextFormField(
                      onSaved: (value) {
                        _titre = value;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Merci de remplir le champ de texte';
                        }
                        return null;
                      },
                    ),
                  ),
                  const Text('Entrez les tags de la tâche', style: TextStyle(height: 3, fontSize: 30)),
                  SizedBox( 
                    width: 200, 
                    child: TextFormField(
                      decoration: const InputDecoration(labelText: 'Séparer les tags par une virgule',),
                      onSaved: (value) {
                        _tags = value;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Merci de remplir le champ de texte';
                        }
                        return null;
                      },
                    ),
                  ),
                  const Text('Entrez le nombre d\'heure de la tâche', style: TextStyle(height: 3, fontSize: 30)),
                  SizedBox( 
                    width: 200, 
                    child: TextFormField(
                      onSaved: (value) {
                        _nbhours = value;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Merci de remplir le champ de texte';
                        }
                        return null;
                      },
                    ),
                  ),
                  const Text('Entrez la difficulté de la tâche', style: TextStyle(height: 3, fontSize: 30)),
                  SizedBox( 
                    width: 200, 
                    child: TextFormField(
                      onSaved: (value) {
                        _nbhours = value;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Merci de remplir le champ de texte';
                        }
                        return null;
                      },
                    ),
                  ),
                  const Text('Entrez une description pour la tâche', style: TextStyle(height: 3, fontSize: 30)),
                  SizedBox( 
                    width: 200, 
                    child: TextFormField(
                      onSaved: (value) {
                        _nbhours = value;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Merci de remplir le champ de texte';
                        }
                        return null;
                      },
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      // Validate returns true if the form is valid, or false otherwise.
                      if (_formKey.currentState!.validate()) {
                        _formKey.currentState!.save();

                        // on créer la nouvelle Tâche
                        Task newTask = Task(
                          id: context.read<TaskViewModel>().getMaxIdTask()+1,
                          title: _titre ?? "titre 1",
                          tags: _tags?.split(',') ?? ["tag1", "tag2"],
                          nbhours: int.tryParse(_nbhours ?? "2") ?? 2,
                          difficulty: int.tryParse(_difficulty ?? "1") ?? 1,
                          description: _description ?? "description 1",
                          favorite: "false"
                        );
                        
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('Tâche créée !'),
                            backgroundColor: Colors.lightGreen,
                            ),
                        );

                        // on créer et ajoute à la liste la tâche
                        context.read<TaskViewModel>().addTask(newTask);

                        // on revient à l'acceuil
                        Navigator.pop(context);
                      }
                    },
                    child: const Text('Valider'),
                  ),
                ],
              ),
            )
        ) 
    );
  }
}