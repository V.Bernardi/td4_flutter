import 'package:flutter/material.dart';
import 'package:td4_2223/card1.dart';
import 'package:td4_2223/card2.dart';
import 'package:td4_2223/card3.dart';
import 'package:td4_2223/settings.dart';
import 'package:td4_2223/formulaire.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;

  static List<Widget> pages = <Widget>[
    Ecran1(),
    Ecran2(),
    const Ecran3(),
    const MyCustomForm(),
    const EcranSettings()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Application TD4',
          style: Theme.of(context).textTheme.titleLarge,
        ),
      ),
      body: pages[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Theme.of(context).textSelectionTheme.selectionColor,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.article, color: Color.fromARGB(255, 57, 55, 128) ,),
            label: 'Card1',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.article, color: Color.fromARGB(255, 57, 55, 128)),
            label: 'Card2',
          ),
          BottomNavigationBarItem(
            icon: 
            Icon(Icons.article,color: Color.fromARGB(255, 57, 55, 128)),
            label: 'Card3',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.article, color: Color.fromARGB(255, 57, 55, 128) ,),
            label: 'Formulaire',
          ),
          BottomNavigationBarItem(icon: Icon(Icons.settings), label: 'Paramètres'),
        ],
      ),
      floatingActionButton: _selectedIndex == 0
          ? FloatingActionButton(
              onPressed: () {
                Navigator.push( context,
                  MaterialPageRoute(builder: (context) => 
                    const MyCustomForm()),
                );
              },
              child: const Icon(Icons.add),
            )
          : const SizedBox.shrink(),
    );
  }
}
