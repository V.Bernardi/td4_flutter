import 'package:flutter/material.dart';

import './models/task.dart';

class Detail extends StatelessWidget{


    final Task task;
    const Detail({required this.task});

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Text(
                    'Detail de la tâche',
                    style: Theme.of(context).textTheme.titleLarge,
                ),
            ),
            body: Container(
                alignment: Alignment.center,
                child: Column(
                    children: <Widget>[
                        Text('Titre : ${task.title}', style: TextStyle(height: 3, fontSize: 30)),
                        Text('Tags : ${task.tags}', style: TextStyle(height: 3, fontSize: 30)),
                        Text('Nombre d\'heure(s) : ${task.nbhours}', style: TextStyle(height: 3, fontSize: 30)),
                        Text('Difficulté : ${task.difficulty}', style: TextStyle(height: 3, fontSize: 30)),
                        Text('Description : ${task.description}', style: TextStyle(height: 3, fontSize: 30)),
                    ]
                )
            ),
        );
    }
}