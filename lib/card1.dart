import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:td4_2223/models/task.dart';
import 'package:td4_2223/viewmodels/task_view_model.dart';


import 'package:td4_2223/detail.dart';
import 'package:td4_2223/edit.dart';

class Ecran1 extends StatelessWidget {
  late List<Task> tasks;
  String tags = '';
  @override
  Widget build(BuildContext context) {
    tasks = context.watch<TaskViewModel>().liste;
    
    // on tri les taches pour mettre les favoris en premier
    // CREER DES DOUBLONS LORS DE L'EDIT D'UNE TACHE
    // tasks.sort((b,a) => a.favorite.compareTo(b.favorite));
    
    return ListView.builder(
        itemCount: tasks.length,
        itemBuilder: (context, index) => Card(
          elevation: 6,
          margin: const EdgeInsets.all(10),
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.lightBlue,
              child: Text(
                tasks[index].id.toString(),
                style: const TextStyle(color: Colors.black),
                ),
            ),
            title: Text(tasks[index].title),
            subtitle: Text(tasks[index].tags.join(" ")),
            trailing :
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    onPressed: (){
                      if (tasks[index].favorite == "true"){
                        context.read<TaskViewModel>().removeFavoris(index);
                      }
                      else{
                        context.read<TaskViewModel>().addFavoris(index);
                      }
                    },
                    icon: 
                    // si la tache est favorite, on affiche un coeur plein
                    // un coeur vide dans le cas contraire
                      tasks[index].favorite == "true"
                    ? const Icon(
                        Icons.favorite,
                        color: Colors.pink,
                        size: 24.0,
                      )
                      : const Icon(
                        Icons.favorite_border,
                        color: Colors.pink,
                        size: 24.0,
                      )

                  ),
                  IconButton(
                    onPressed: (){
                      Navigator.push( context,
                        MaterialPageRoute(builder: (context) => 
                          Edit(task: tasks[index])),
                      );
                    },
                    icon: const Icon(Icons.edit_note, size : 32),
                  ),
                  IconButton(
                    onPressed: (){
                      context.read<TaskViewModel>().supprimerTache(index);

                    },
                    icon: const Icon(Icons.delete, size : 32),
                  ), 
                ]
                
              ),
            onTap: (){
              Navigator.push( context,
                MaterialPageRoute(builder: (context) => 
                  Detail(task: tasks[index])),
              );
            }
          ),
         )
     );
  }
}