import 'package:flutter/material.dart';
import 'package:td4_2223/models/task.dart';

class TaskViewModel extends ChangeNotifier {
  late List<Task> liste; // l'initialisation se fera plus tard

  TaskViewModel() {
    liste = [];
  }
  
  void addTask(Task task) {
    liste.add(task);
    notifyListeners(); // pour notifier les listeners (maj)
  }

  void generateTasks() {
    liste = Task.generateTask(50);
    notifyListeners();
  }

  void supprimerTache(int index) {
    liste.removeAt(index);
    notifyListeners();
  }

  void editTask(Task tache) {
    liste[tache.getId()] = tache;
    notifyListeners();
  }

  int getMaxIdTask() {
    int max = 0;
    for (int i = 0; i < liste.length; i++) {
      if (liste[i].getId() > max) {
        max = liste[i].getId();
      }
    }
    notifyListeners();
    return max;
  }

  //favoris
  void addFavoris(int index) {
    liste[index].favorite = "true";
    notifyListeners();
  }

  void removeFavoris(int index) {
    liste[index].favorite = "false";
    notifyListeners();
  }

}